package com.example.petersam.firebase.adapter_holder;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.petersam.firebase.R;
import com.example.petersam.firebase.model.Karyawan;

import java.text.DecimalFormat;

public class ViewHolderKaryawan extends RecyclerView.ViewHolder {
    public Karyawan listKaryawan;
    public LinearLayout layoutKaryawanCard;
    private TextView textKaryawan, textDivisi, textGaji;

    public ViewHolderKaryawan(@NonNull View itemView) {
        super(itemView);

        this.layoutKaryawanCard = (LinearLayout) itemView.findViewById(R.id.layoutListKaryawan);
        this.textKaryawan = (TextView) itemView.findViewById(R.id.textKaryawan);
        this.textDivisi = (TextView) itemView.findViewById(R.id.textDivisi);
        this.textGaji = (TextView) itemView.findViewById(R.id.textGaji);
    }

    public void setListModel(Karyawan listModel) {
        this.listKaryawan = listModel;

        this.textKaryawan.setText(listModel.getNama_karyawan());
        this.textDivisi.setText(listModel.getDivisi_karyawan());
        this.textGaji.setText("Rp. " + numberFormat(listModel.getGaji_karyawan()));
    }

    public static String numberFormat(int source) {
        if (source == -1) return "";
        DecimalFormat formatter = new DecimalFormat("#,###,###");
        String yourFormattedString = formatter.format(source);
        return yourFormattedString.replaceAll(",", ".");
    }
}
