package com.example.petersam.firebase;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.petersam.firebase.adapter_holder.AdapterKaryawan;
import com.example.petersam.firebase.model.Karyawan;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements AdapterKaryawan.FirebaseDataListener {

    AdapterKaryawan adapterKaryawan;
    private Button btnDaftar;
    private EditText inputNama,inputDivisi, inputGaji;
    private String valueNama,valueDivisi,valueGaji;

    private RecyclerView listKaryawan;

    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;

    private RecyclerView recyclerView;
    private List<Karyawan> karyawanList = new ArrayList<>();

    Context mContext;

    private DatabaseReference database;
    private ArrayList<Karyawan> daftarKaryawan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //        memulai resources yang ada di class FirebaseApp
        FirebaseApp.initializeApp(this);

        btnDaftar     = (Button)   findViewById(R.id.btnDaftar);
        inputNama     = (EditText) findViewById(R.id.inputNama);
        inputGaji     = (EditText) findViewById(R.id.inputGaji);
        inputDivisi   = (EditText) findViewById(R.id.inputDivisi);

        listKaryawan  = (RecyclerView) findViewById(R.id.listKaryawan);
        listKaryawan.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        listKaryawan.setLayoutManager(layoutManager);

        database = FirebaseDatabase.getInstance().getReference();
//        Akan memanggil child yang bernama karyawan
        database.child("karyawan").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
//                 Saat ada data baru, masukkan datanya ke ArrayList
                daftarKaryawan = new ArrayList<>();
                /*Pengulangan untuk memanggil data dengan dimensi berbeda di dalam kelas DataSnapshoot
                        dinamakan child*/
                for (DataSnapshot noteDataSnapshot : dataSnapshot.getChildren()) {
                    Karyawan karyawan= noteDataSnapshot.getValue(Karyawan.class);
                    karyawan.setKey(noteDataSnapshot.getKey());
                    daftarKaryawan.add(karyawan);
                }
                /*Mulai mengisi adapter sesuai dengan list karyawan dengan adapter dan
                        pengaturan sesuai yang di minta adapter */
                adapter = new AdapterKaryawan(daftarKaryawan, MainActivity.this);
                listKaryawan.setAdapter(adapter);
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
                Toast.makeText(MainActivity.this, "status onCacelled", Toast.LENGTH_SHORT).show();
            }
        });

//        Untuk Edit Data dan Mapping data sesuai wizard
        final Karyawan karyawan= (Karyawan) getIntent().getSerializableExtra("data");
        if (karyawan != null) {
            inputNama.setText(karyawan.getNama_karyawan());
            inputDivisi.setText(karyawan.getDivisi_karyawan());
            inputGaji.setText(String.valueOf(karyawan.getGaji_karyawan()));
            btnDaftar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    karyawan.setNama_karyawan(inputNama.getText().toString());
                    karyawan.setDivisi_karyawan(inputDivisi.getText().toString());
                    karyawan.setGaji_karyawan(Integer.parseInt(String.valueOf(inputGaji.getText())));

                    updateKaryawan(karyawan);
                }
            });
        } else {
            btnDaftar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    validation();
                }
            });
        }
    }

    private void validation() {
        valueNama     = inputNama.getText().toString();
        valueGaji     = inputGaji.getText().toString();
        valueDivisi   = inputDivisi.getText().toString();

        boolean isEmptyText = false;

        if(TextUtils.isEmpty(valueNama)){
            isEmptyText = true;
            inputNama.setError(getString(R.string.note_fill_blank));
        }
        if(TextUtils.isEmpty(valueDivisi)){
            isEmptyText = true;
            inputDivisi.setError(getString(R.string.note_fill_blank));
        }
        if(TextUtils.isEmpty(valueGaji)){
            isEmptyText = true;
            inputGaji.setError(getString(R.string.note_fill_blank));
        }
        if (!isEmptyText){
            daftarkanKaryawan();
            Toast.makeText(getBaseContext(), "Daftar karyawan berhasil tidak ada yang kosong", Toast.LENGTH_SHORT).show();
        }
    }

    private void daftarkanKaryawan() {
        submitKaryawan(new Karyawan(
                inputNama.getText().toString(),
                inputDivisi.getText().toString(),
                Integer.parseInt(String.valueOf(inputGaji.getText()))));
        Toast.makeText(this, "Berhasil Daftar, All Filled !!!", Toast.LENGTH_LONG).show();

        InputMethodManager imm = (InputMethodManager)
                getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(
                inputNama.getWindowToken(), 0);
    }

    private void submitKaryawan(Karyawan karyawan) {
        database.child("karyawan").push()
                .setValue(karyawan).addOnSuccessListener(this, new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                inputNama.setText("");
                inputDivisi.setText("");
                inputGaji.setText("");
                Snackbar.make(findViewById(R.id.btnDaftar), "Employee successfully registered.", Snackbar.LENGTH_LONG).show();
            }
        });
    }

    private void updateKaryawan(Karyawan karyawan) {
        database.child("karyawan") //akses parent index, ibaratnya seperti nama tabel
                .child(karyawan.getKey()) //select karyawan berdasarkan key
                .setValue(karyawan) //set value karyawan yang baru
                .addOnSuccessListener(this, new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Snackbar.make(findViewById(R.id.btnDaftar), "Employee successfully updated. !",
                                Snackbar.LENGTH_LONG).setAction("See", new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                finish();
                            }
                        }).show();
                    }
                });
    }

    @Override
    public void onDeleteData(Karyawan karyawan, int position) {
        if(database!=null){
            database.child("karyawan").child(karyawan.getKey())
                    .removeValue()
                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            Toast.makeText(MainActivity.this,
                                    "Employee successfully deleted.",
                                    Toast.LENGTH_LONG).show();
                        }
                    });
        }
    }

    private boolean isEmpty(String s){
        return TextUtils.isEmpty(s);
    }

    public static Intent getActIntent(Activity activity) {
        return new Intent(activity, MainActivity.class);
    }

    void showExitDialogConfirmation() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Anda Yakin ingin keluar aplikasi ini ?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        finish();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
        Button positive = alert.getButton(AlertDialog.BUTTON_POSITIVE);
        positive.setTextColor(Color.BLACK);
        Button negative = alert.getButton(AlertDialog.BUTTON_NEGATIVE);
        negative.setTextColor(Color.BLACK);
    }

    @Override
    public void onBackPressed() {
        /*super.onBackPressed();*/
        boolean isCanShowAlertDialog = false;
        List<Fragment> fragmentList = getSupportFragmentManager().getFragments();
        if (fragmentList != null) {
            //TODO: Perform your logic to pass back press here
            for (Fragment fragment : fragmentList) {
                if (fragment instanceof OnBackPressedListener) {
                    isCanShowAlertDialog = true;
                    ((OnBackPressedListener) fragment).onBackPressed();
                }
            }
        }
        if (!isCanShowAlertDialog) {
            showExitDialogConfirmation();
        }
    }
}
