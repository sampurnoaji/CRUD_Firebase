package com.example.petersam.firebase.adapter_holder;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.petersam.firebase.MainActivity;
import com.example.petersam.firebase.R;
import com.example.petersam.firebase.model.Karyawan;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class AdapterKaryawan extends RecyclerView.Adapter<AdapterKaryawan.MyViewHolder> {

    private ArrayList<Karyawan> models;
    private Context mContext;
    FirebaseDataListener listener;

    public AdapterKaryawan(ArrayList<Karyawan> models, Context mContext) {
        //        Inisiasi data dan variabel yang akan digunakan
        this.models = models;
        this.mContext = mContext;
        this.listener = (MainActivity) mContext;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_karyawan, viewGroup, false);
        return new AdapterKaryawan.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, final int i) {
        final Karyawan model = models.get(i);
        myViewHolder.setListModel(model);

        myViewHolder.layoutKaryawanCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(mContext, "Detail Data siap", Toast.LENGTH_SHORT).show();
            }
        });

        myViewHolder.layoutKaryawanCard.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
//                Toast.makeText(mContext, "YEYEYE On Long Click Listener", Toast.LENGTH_SHORT).show();
                final Dialog dialog = new Dialog(mContext);
                dialog.setContentView(R.layout.layout_dialog);
                dialog.setTitle("Pilih Aksi");
                dialog.show();

                Button editButton = (Button) dialog.findViewById(R.id.bt_edit_data);
                Button delButton = (Button) dialog.findViewById(R.id.bt_delete_data);

                //apabila tombol edit diklik
                editButton.setOnClickListener(
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                dialog.dismiss();
                                mContext.startActivity(MainActivity.getActIntent((Activity)
                                        mContext).putExtra("data", models.get(i)));
                            }
                        }
                );

                //apabila tombol delete diklik
                delButton.setOnClickListener(
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                dialog.dismiss();
                                listener.onDeleteData(models.get(i), i);
                            }
                        }
                );

                return true;
            }
        });
    }

    @Override
    public int getItemCount() {
        if (models != null){
            return models.size();
        }
        return 0;
    }

    public interface FirebaseDataListener{
        void onDeleteData(Karyawan karyawan, int position);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public Karyawan listKaryawan;
        public LinearLayout layoutKaryawanCard;
        private TextView textKaryawan, textDivisi, textGaji;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            this.layoutKaryawanCard = (LinearLayout) itemView.findViewById(R.id.layoutListKaryawan);
            this.textKaryawan = (TextView) itemView.findViewById(R.id.textKaryawan);
            this.textDivisi   = (TextView) itemView.findViewById(R.id.textDivisi);
            this.textGaji     = (TextView) itemView.findViewById(R.id.textGaji);
        }
        public void setListModel(Karyawan listModel) {
            this.listKaryawan = listModel;

            this.textKaryawan.setText(listModel.getNama_karyawan());
            this.textDivisi.setText(listModel.getDivisi_karyawan());
            this.textGaji.setText("Rp. " + numberFormat(listModel.getGaji_karyawan()));
        }
        private String numberFormat(int source) {
            if (source == -1) return "";
            DecimalFormat formatter = new DecimalFormat("#,###,###");
            String yourFormattedString = formatter.format(source);
            return yourFormattedString.replaceAll(",", ".");
        }
    }
}
