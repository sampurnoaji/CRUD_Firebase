package com.example.petersam.firebase;

public interface OnBackPressedListener {
    void onBackPressed();
}
